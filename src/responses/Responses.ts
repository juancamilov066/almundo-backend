import { injectable } from "inversify";

@injectable()
export class Responses {

    public dataResponse<T>(data: T): any {
        return {
            code: 200,
            data: data,
            success: true,
            message: 'Data queried successfully'
        }
    }

    public badRequestResponse(errors: any, message?: string): any {
        return {
            code: 400,
            errors: errors,
            success: true,
            message: message !== undefined ? message : 'Data queried successfully'
        }
    }

    public notFoundResponse(id: number): any {
        return {
            code: 404,
            data: undefined,
            success: false,
            message: `Resource with id ${id} wasn't found`
        }
    }

    public updatedResponse(id: number): any {
        return {
            code: 200,
            data: id,
            success: true,
            message: `Resource with id ${id} was updated successfully`
        }
    }

    public createdResponse(id: number): any {
        return {
            code: 200,
            data: id,
            success: true,
            message: `Resource with id ${id} was created successfully`
        }
    }

    public deletedResponse(id: number): any {
        return {
            code: 200,
            data: id,
            success: true,
            message: `Resource with id ${id} was deleted successfully`
        }
    }

}