import * as express from 'express';
import { injectable, inject } from 'inversify';
import { TYPES } from '../types';
import { AppController } from '../interfaces/AppController';  
import { Responses } from '../responses/Responses';
import HotelServiceImpl from '../services/HotelServiceImpl';
import Hotel from '../schemas/Hotel.Schema';
import { errorFields, isNull, stringToNumberArray } from '../utils/Utils';
import BadRequest from '../exceptions/BadRequest';
import NotFound from '../exceptions/NotFound';
import InternalServerError from '../exceptions/InternalServerError';

const { check, validationResult, body  } = require('express-validator/check');

@injectable()
export default class HotelController implements AppController {
    
    @inject(TYPES.HotelService)
    private hotelService: HotelServiceImpl;

    @inject(TYPES.Responses)
    private responses: Responses;

    register(app: express.Application): void {

        app.post('/hotel', async(req: express.Request, res: express.Response, next: express.NextFunction) => {

            try {

                const hotel = req.body.hotel;
                const created: number = await this.hotelService.upsert(hotel);

                if (created === undefined) throw new InternalServerError('Hotel not created');
                else res.status(200).json(this.responses.createdResponse(created));

            } catch (error) {
                next(error);
            }

        });

        app.put('/hotel', async(req: express.Request, res: express.Response, next: express.NextFunction) => {

            try {

                const hotel = req.body.hotel;
                const updated: number = await this.hotelService.upsert(hotel);

                if (updated === undefined) throw new InternalServerError('Hotel not updated');
                else res.status(200).json(this.responses.updatedResponse(updated));

            } catch (error) {
                next(error);
            }

        });

        app.delete('/hotel/:id', [
            check('id').isInt()
        ], async(req: express.Request, res: express.Response, next: express.NextFunction) => {

            try {

                const errors = errorFields(req);
                if (errors.length > 0) throw new BadRequest('Id must be int');

                const id = req.params.id;
                const deleted: number = await this.hotelService.delete(id);

                if (deleted === undefined) throw new InternalServerError('Hotel not deleted');
                else res.status(200).json(this.responses.deletedResponse(deleted));

            } catch (error) {
                next(error);
            }
        
        });

        app.get('/hotel', [
            check('page').isInt({min: 0}),
            check('sendJson').isInt()
        ], async(req: express.Request, res: express.Response, next: express.NextFunction) => {
            try {
                const errors = errorFields(req);
                if (errors.length > 0) throw new BadRequest('Page and sendJson is required');

                const page = req.query.page;
                const name = isNull(req.query.name) ? '' : req.query.name;
                const stars = isNull(req.query.stars) ? undefined : req.query.stars;
                const sendJson = req.query.sendJson;

                if (sendJson == 1) {
                    const hotels: Array<Hotel> = await this.hotelService.findAllJSON(page, name, stringToNumberArray(stars));
                    return res.status(200).json(this.responses.dataResponse(hotels));
                } else {
                    const hotels: Array<Hotel> = await this.hotelService.findAll(page, name, stringToNumberArray(stars));
                    return res.status(200).json(this.responses.dataResponse(hotels));
                }
            } catch (error) {
                next(error);
            }
        });

        app.get('/hotel/:id', [
            check('sendJson').isInt(),
            check('id').isInt()
        ], async(req: express.Request, res: express.Response, next: express.NextFunction) => {
            try {
                const errors = errorFields(req);
                if (errors.length > 0) throw new BadRequest('sendJson is required and id must be int');
                
                const id = req.params.id;
                const sendJson = req.query.sendJson;
                
                if (sendJson === 1) {
                    const hotel: Hotel = await this.hotelService.findOneJSON(id);
                    if (hotel === null || hotel === undefined) throw new NotFound('Hotel not found');
                    return res.status(200).json(this.responses.dataResponse(hotel));
                } else {
                    const hotel: Hotel = await this.hotelService.findOne(id);
                    if (hotel === null || hotel === undefined) throw new NotFound('Hotel not found');
                    return res.status(200).json(this.responses.dataResponse(hotel));
                }
            } catch (error) {
                next(error);
            }
        });
    }

}