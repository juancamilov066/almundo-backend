import App from "./app";

console.info(`
  AAA    lll                                      dd        
 AAAAA   lll  mm mm mmmm   uu   uu  nn nnn        dd   oooo  
AA   AA  lll  mmm  mm  mm  uu   uu  nnn  nn   dddddd  oo  oo 
AAAAAAA  lll  mmm  mm  mm  uu   uu  nn   nn  dd   dd  oo  oo 
AA   AA  lll  mmm  mm  mm   uuuu u  nn   nn   dddddd   oooo                                                                                                                                        
`);

process.on('uncaughtException', (err) => {
    console.error('Unhandled Exception ', err.message);
});

process.on('unhandledRejection', (err: Error, promise: Promise<any>) => {
    console.error('Unhandled Rejection ', err.message);
});

const app: App = new App();
app.start();
module.exports = app;