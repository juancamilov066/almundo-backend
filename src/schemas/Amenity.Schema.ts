
import HotelAmenities from "./HotelAmenities.Schema";
import Hotel from './Hotel.Schema';
import {Entity, Column, PrimaryGeneratedColumn, ManyToMany, JoinTable, OneToMany} from "typeorm";

@Entity('amenity')
export default class Amenity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    createdAt: Date;

    @Column()
    updatedAt: Date;
    
}