import {Entity, Column, PrimaryGeneratedColumn, ManyToMany, JoinTable, OneToMany} from "typeorm";
import Amenity from './Amenity.Schema';
import HotelAmenities from './HotelAmenities.Schema';

@Entity('hotel')
export default class Hotel {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    stars: number;

    @Column()
    image: string;

    @Column()
    price: number;

    @Column()
    createdAt: Date;

    @Column()
    updatedAt: Date;

    @ManyToMany(type => Amenity)
    @JoinTable({
        name: 'hotel_amenities'
    })
    amenities: Amenity[];

    /*@OneToMany(type => HotelAmenities, hotelAmenities => hotelAmenities.hotel, {
        eager: true
    })
    amenities: HotelAmenities[];*/

}