const TYPES = {
    Hotel: Symbol.for("Hotel"),
    Amenity: Symbol.for("Amenity"),
    AppController: Symbol.for('AppController'),
    HotelService: Symbol.for("HotelService"),
    HotelDAO: Symbol.for("HotelDAO"),
    Responses: Symbol.for("Responses")
};
 
export { TYPES };