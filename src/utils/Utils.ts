const { validationResult } = require('express-validator/check');

export function errorFields(req): Array<any> {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return errors.array();
    }
    return new Array();
}

export function isNull(str) {
    return (!str || /^\s*$/.test(str) || 0 === str.length);
}

export function stringToNumberArray(stringArray: Array<string>) {
    return stringArray !== undefined ? stringArray.map(Number) : undefined;
}

export function getHotelJSONDataFile<T>(): Promise<T> {
    const fs = require('fs');
    return new Promise<T>((resolve, reject) => {
        fs.readFile('./data.json', 'utf-8', (error, data) => {
            error ? reject(error) : resolve(JSON.parse(data));
        });
    });
}