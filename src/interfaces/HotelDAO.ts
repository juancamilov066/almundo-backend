import Hotel from "../schemas/Hotel.Schema";
import Amenity from "../schemas/Amenity.Schema";

export interface HotelDAO {

    upsert(hotel: Hotel): Promise<number>;
    delete(id: number): Promise<number>;

    findAll(page: number, name: string, stars: Array<number>): Promise<Array<Hotel>>;
    findAllJSON(page: number, name: string, stars: Array<number>): Promise<Array<Hotel>>;
    findOne(id: number): Promise<Hotel>;
    findOneJSON(id: number): Promise<Hotel>;
    findAmenity(hotelId: number, name: string): Promise<Amenity>;
    findAmenityJSON(hotelId: number, name: string): Promise<string>;

}