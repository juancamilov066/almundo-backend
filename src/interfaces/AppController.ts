import * as express from 'express';

export interface AppController {

    register(app: express.Application): void;

}