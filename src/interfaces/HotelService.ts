import Hotel from "../schemas/Hotel.Schema";

export interface HotelService {

    upsert(hotel: Hotel): Promise<number>;
    delete(id: number): Promise<number>;

    findAll(page: number, name: string, stars: Array<number>): Promise<Array<Hotel>>;
    findAllJSON(page: number, name: string, stars: Array<number>): Promise<Array<Hotel>>;
    findOne(id: number): Promise<Hotel>;
    findOneJSON(id: number): Promise<Hotel>;
    findHotelAmenity(hotelId: number, name: string): Promise<any>;
    findHotelAmenityJSON(hotelId: number, name: string): Promise<any>;
    
}