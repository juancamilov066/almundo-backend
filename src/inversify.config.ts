import { Container } from 'inversify';
import { TYPES } from './types';
import HotelController from './controllers/Hotel.Controller';
import { HotelService } from './interfaces/HotelService';
import HotelServiceImpl from './services/HotelServiceImpl';
import { HotelDAO } from './interfaces/HotelDAO';
import HotelDAOImpl from './daos/HotelDAOImpl';
import { Responses } from './responses/Responses';

const container = new Container();

//Controllers
container.bind<HotelController>(TYPES.AppController).to(HotelController).inSingletonScope();

// Services
container.bind<HotelService>(TYPES.HotelService).to(HotelServiceImpl).inSingletonScope();

// DAOS
container.bind<HotelDAO>(TYPES.HotelDAO).to(HotelDAOImpl).inSingletonScope();

//Others
container.bind<Responses>(TYPES.Responses).to(Responses).inSingletonScope();


export default container;