import Hotel from "../schemas/Hotel.Schema";
import { injectable, inject } from "inversify";
import { HotelService } from "../interfaces/HotelService";
import { TYPES } from "../types";
import HotelDAOImpl from "../daos/HotelDAOImpl";
import Amenity from "../schemas/Amenity.Schema";

@injectable()
export default class HotelServiceImpl implements HotelService {

    @inject(TYPES.HotelDAO)
    private hotelDao: HotelDAOImpl;

    upsert(hotel: Hotel): Promise<number> {
        return this.hotelDao.upsert(hotel);
    }
    
    delete(id: number): Promise<number> {
        return this.hotelDao.delete(id);
    }

    findAllJSON(page: number, name: string, stars: Array<number>): Promise<Hotel[]> {
        return this.hotelDao.findAllJSON(page, name, stars);
    }

    findAll(page: number, name: string, stars: Array<number>): Promise<Hotel[]> {
        return this.hotelDao.findAll(page, name, stars);
    }

    findOne(id: number): Promise<Hotel> {
        return this.hotelDao.findOne(id);
    }

    findOneJSON(id: number): Promise<Hotel> {
        return this.hotelDao.findOneJSON(id);
    }

    findHotelAmenity(id: number, name: string): Promise<Amenity> {
        return this.hotelDao.findAmenity(id, name);
    }
    
    findHotelAmenityJSON(id: number, name: string): Promise<String> {
        return this.hotelDao.findAmenityJSON(id, name);
    }

}