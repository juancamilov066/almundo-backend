
import "reflect-metadata";

import * as express from 'express';
import * as morgan from 'morgan';
import * as cors from 'cors';
import { TYPES } from "./types";
import { inject } from "inversify";
import { AppController } from './interfaces/AppController';
import container from './inversify.config';
import { createConnection } from 'typeorm';
import { DatabaseOptions } from './config/DatabaseConnection';
import NotFound from "./exceptions/NotFound";
import BadRequest from "./exceptions/BadRequest";

require('dotenv').load();

export default class App {

    private async init() {
                
        const app: express.Application = express();
        await createConnection(DatabaseOptions);

        app.set('port', process.env.PORT || 3000);
        app.use('/assets/hotel', express.static('static/hotels'));
        app.use('/assets/amenity', express.static('static/amenities'));
        app.use(morgan('dev'));
        app.use(cors({ optionsSuccessStatus: 200 }));

        const controllers: AppController[] = container.getAll<AppController>(TYPES.AppController);
        controllers.forEach(controller => controller.register(app));

        app.use((err: Error, req: express.Request, res: express.Response, next: express.NextFunction) => {
            if (err instanceof NotFound) {
                return res.status(404).json({
                    code: 404,
                    success: false,
                    message: err.message
                });
            }
            if (err instanceof BadRequest) {
                return res.status(400).json({
                    code: 400,
                    success: false,
                    message: err.message
                });
            }
            next(err);
        });

        app.use((err: Error, req: express.Request, res: express.Response, next: express.NextFunction) => {
            return res.status(500).json({
                code: 500,
                success: false,
                message: 'Internal server error, try again later'
            });
        });

        return Promise.resolve(app);
    }

    public async start() {
        const app = await this.init();
        const server = app.listen(app.get('port'), () => {
            console.info(('Almundo server running at port %d in %s mode'), app.get('port'), app.get('env'));
        });
        return Promise.resolve(server);
    }
}