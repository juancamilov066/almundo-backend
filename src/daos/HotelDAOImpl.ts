import { HotelDAO } from "../interfaces/HotelDAO";
import Hotel from "../schemas/Hotel.Schema";
import { injectable } from "inversify";
import { Repository, getRepository } from "typeorm";
import { getHotelJSONDataFile, isNull } from '../utils/Utils'
import NotFound from "../exceptions/NotFound";
import Amenity from "../schemas/Amenity.Schema";

@injectable()
export default class HotelDAOImpl implements HotelDAO {

    private hotelRepository: Repository<Hotel>;
    private readonly PAGE_SIZE: number = 10;

    constructor() {
        this.hotelRepository = getRepository(Hotel);
    }

    async upsert(hotel: Hotel): Promise<number> {
        const saved = await this.hotelRepository.save(hotel);
        if (saved !== undefined) {
            return Promise.resolve(saved.id);
        }
        return Promise.resolve(undefined);
    }

    async delete(id: number): Promise<number> {
        const updated = await this.hotelRepository.delete(id);
        if (updated !== undefined) {
            return Promise.resolve(id);
        }
        return Promise.resolve(undefined);
    }

    async findAmenity(hotelId: number, name: string): Promise<Amenity> {
        const hotel = await this.findOne(hotelId);
        return new Promise<Amenity>((resolve, reject) => {
            const amenities: Amenity[] = hotel.amenities.filter(amenity => {
                return amenity.name === name;
            });
            resolve(amenities.length > 0 ? amenities[0] : undefined);
        })
    }

    async findAmenityJSON(hotelId: number, name: string): Promise<string> {
        const hotel = await this.findOneJSON(hotelId);
        return new Promise<string>((resolve, reject) => {
            const amenities: any[] = hotel.amenities.filter(amenity => {
                return amenity.name === name;
            });
            resolve(amenities.length > 0 ? amenities[0].name : undefined);
        })
    }

    async findAllJSON(page: number, name: string, stars: Array<number>): Promise<Array<Hotel>> {
        const hotels = await getHotelJSONDataFile<Hotel[]>();
        const filtered = await this.filterJSONHotels(hotels, page, name, stars);
        return this.filterJSONHotels(hotels, page, name, stars);;
    }

    async findOneJSON(id: number): Promise<Hotel> {
        const hotels = await getHotelJSONDataFile<Hotel[]>();
        return new Promise<Hotel>((resolve, reject) => {
            for (let i = 0; i < hotels.length; i++) {
                const hotel = hotels[i];
                if (hotel.id === id) {
                    resolve(hotel);
                }
            }
            reject(undefined);
        });
    }

    async findAll(page: number, name: string, stars: Array<number>): Promise<Hotel[]> {
        return this.hotelRepository
            .createQueryBuilder("hotel")
            .leftJoinAndSelect("hotel.amenities", "amenities")
            .where('hotel.name LIKE :name', {name: `%${name}%`})
            .andWhere('hotel.stars IN (:stars)', {stars: stars !== undefined ? stars : [1,2,3,4,5]})
            .skip(page * this.PAGE_SIZE)
            .take(this.PAGE_SIZE)
            .getMany();
    }

    async findOne(id: number): Promise<Hotel> {
        return this.hotelRepository
            .findOne(id, { relations: ["amenities"] })
    }

    private filterJSONHotels(hotels: Array<Hotel>, page: number, name: string, stars: Array<number>): Promise<Array<Hotel>> {
        return new Promise<Array<Hotel>>((resolve, reject) => {
            const hotelsResponse = new Array<Hotel>();
            for (let i = 0; i < hotels.length; i++) {
                const hotel = hotels[i];
                if (!isNull(name) && stars !== undefined && stars.length > 0) {
                    if (hotel.name.toLowerCase().includes(name.toLowerCase()) && stars.indexOf(hotel.stars) >= 0) {
                        hotelsResponse.push(hotel);
                    }
                } else if (stars !== undefined && stars.length > 0) {
                    if (stars.indexOf(hotel.stars) >= 0) {
                        hotelsResponse.push(hotel);
                    }
                } else if (!isNull(name)) {
                    if (hotel.name.toLowerCase().includes(name.toLowerCase())) {
                        hotelsResponse.push(hotel);
                    }
                } else {
                    hotelsResponse.push(hotel);
                }
            }
            resolve(hotelsResponse.slice(page * this.PAGE_SIZE, (page * this.PAGE_SIZE) + this.PAGE_SIZE));
        });
    }

}