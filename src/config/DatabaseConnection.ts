import Amenity from '../schemas/Amenity.Schema';
import Hotel from '../schemas/Hotel.Schema';
import { ConnectionOptions } from 'typeorm';
import HotelAmenities from '../schemas/HotelAmenities.Schema';

export const DatabaseOptions: ConnectionOptions = {
    type: "mysql",
    host: process.env.HOST || 'localhost',
    port: Number(process.env.PORT) || 3306,
    username: process.env.DB_USER || 'root',
    password: process.env.DB_PASS || 'root',
    database: process.env.DB_NAME || 'almundo_db',
    entities: [
        Hotel,
        Amenity,
        HotelAmenities
    ],
    synchronize: true,
    logging: true
}