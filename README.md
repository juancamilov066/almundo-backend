# Almundo Backend End - Prueba

Hola, bienvenido a la prueba de AlMundo.
Este repositorio contiene el proyecto back end de la prueba

## Instrucciones para ejecutar

Antes que nada, ejecuta el comando `npm install` para instalar todas las dependencias

Como se uso el registro activo de TypeORM para hacer los puntos extras de la prueba, se adjuntan los scripts de base de datos para la creacion de las tablas en MySQL.
Se debe crear la base de datos de almundo_db

Primero debes tener un conexion activa en localhost:3306, se puede lograr instalando un servidor WAMP, XAMP o MAMP

Luego de eso, debes ejecutar el script 'almundo_db.sql' para crear la base de datos.
Por defecto, esta configurado asi

Configuracion  | Valor
------------- | -------------
DB_NAME | almundo_db
DB_HOST | localhost
DB_PORT | 3306
DB_USER | root
DB_PASS | root

Lo puedes modificar para produccion en el archivo .env o directamente en src/config/DatabaseConnection.ts

Finalmente, ejecuta el comando de NPM `npm run serve` y el servidor correra en `localhost:3000`. Asegurate de tener NPM instalado.

## Informacion Extra

* El proyecto se trabajo con Typescript para tener una mejor estructura, que luego de hacer build, se transforma a Js.
* Se uso inversify para inyeccion de dependencias, esta configuracion se puede ver en el archivo src/inversify.config.ts.


